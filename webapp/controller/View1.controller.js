sap.ui.define([
	"sap/ui/core/mvc/Controller"
], function (Controller) {
	"use strict";

	return Controller.extend("Table.Table.controller.View1", {
		onInit: function () {
			var oTable = this.getView().byId("abc");
			var oModel = new sap.ui.model.json.JSONModel();
			oModel.loadData("model/Object.json");
			oTable.setModel(oModel);
		}
	});
});